import express from 'express'
import http from 'http'
import { Server, Socket } from 'socket.io'
import { Player, Bid } from './types/types'
import { dealCards } from './game'
import { evaluateBid } from './evaluateBid'
import { rotate } from './utils/rotate'
import { gamesHierarchy } from './gameObjects'

const app = express()
const server = http.createServer(app)
export const io = new Server(server)

app.use(express.static('public'))
app.use(function (req, res, next) {
  res.setHeader(
    'Content-Security-Policy-Report-Only',
    "default-src 'self'; font-src 'self'; img-src 'self'; script-src 'self'; style-src 'self'; frame-src 'self'"
  )
  next()
})

app.get('/', (req, res) => {
  res.sendFile('index.html', { root: 'public' })
})

const players: Player[] = []
const gameChoices: Bid[] = []

io.on('connection', async (socket: Socket) => {
  const room = 'Wirtschaft'
  console.log(`${socket.id} connected.`)
  const membersInRoom = await io.in(room).fetchSockets()
  if (membersInRoom.length >= 4) {
    console.log(`Room ${room} is full.`)
    socket.disconnect
    console.log(`${socket.id} disconnected.`)
  } else {
    socket.join(room)
    const player = { id: socket.id, hand: [] }
    players.push(player)
    io.to(room).emit('playerJoined', socket.id, players)
  }

  if (players.length === 4) {
    console.log('There are 4 players now. Starting game.')
    dealCards(players)
    io.to(room).emit('askIfGame', 'Spiel?')
  }

  socket.on('hasGame?', (arg) => {
    gameChoices.push(arg)
    if (gameChoices.length === 4) {
      if (gameChoices.every((bid) => bid.hasGame === false)) {
        console.log('No games, dealing new cards.')
        // TODO: make configurable (Bsp. Ramsch...)
        gameChoices.length = 0
        dealCards(players)
        io.to(room).emit('askIfGame', 'Spiel?')
      } else {
        console.log('Evaluating bid..')
        console.log(gameChoices)
        // TODO: Evaluate if the game can be played with the cards the player has!
        const winnerBid = evaluateBid(gameChoices)
        console.log(`WINNER BID: ${JSON.stringify(winnerBid)}`)
        if (winnerBid === undefined) {
          throw new Error("That shouldn't have happened...")
        }
        if (winnerBid.game?.includes('Farb')) {
          io.to(winnerBid.player).emit('askForSuit', 'Welche Farbe?')
        } else if (winnerBid?.game === 'Sauspiel') {
          io.to(winnerBid.player).emit('askForSau', 'Auf welche Sau?')
        } else if (winnerBid?.game === 'Wenz') {
          const msg = `${winnerBid.player} spielt einen Wenz.`
          startTricks(msg)
        } else if (winnerBid?.game === 'Geier') {
          const msg = `${winnerBid.player} spielt einen Wenz.`
          startTricks(msg)
        } else {
          // start tricks
        }
        // io.to(room).emit('gameDecided', winnerBid)
      }
    }
  })

  socket.on('whichSuit?', (arg) => {
    const msg = `${arg.player} spielt ein ${arg.suit}solo.`
    startTricks(msg)
  })

  socket.on('whichSau?', (arg) => {
    const msg = `${arg.player} spielt auf die ${arg.suit}sau.`
    startTricks(msg)
  })

  socket.on('myOwnId?', () => {
    const msg = `${socket.id}`
    io.to(socket.id).emit('yourOwnId', msg)
  })

  socket.on('seating?', (players) => {
    const relativeSeating = rotate(
      players,
      players.length - players.map((player: any) => player.id).indexOf(socket.id)
    )
    io.to(socket.id).emit('seatingSelfPov', relativeSeating)
  })

  const startTricks = (msg: string) => {
    io.to(room).emit('startTricks', msg, `${players[0].id} kommt raus.`)
  }

  socket.on('disconnect', () => {
    console.log(`${socket.id} disconnected.`)
  })
})

server.listen(3000, () => {
  console.log('Server listening on port 3000.')
})

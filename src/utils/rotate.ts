export function rotate(arr: any[], times: number): any {
  let result: any[] = []
  const innerRotate = (arr: any[]) => {
    return arr.slice(-1).concat(arr.slice(0, -1))
  }
  result = innerRotate(arr)
  for (let i = 1; i < times; i++) {
      result = innerRotate(result)
  }
  return result
}

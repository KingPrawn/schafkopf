import jsonfile from 'jsonfile'

export const logToFile = (msg: Record<string, unknown>) => {
  const file = 'events.json'

  jsonfile
    .writeFile(file, msg)
    .then(() => {
      console.log('write complete.')
    })
    .catch((err) => console.error(err))
}

logToFile({ hello: 'world' })

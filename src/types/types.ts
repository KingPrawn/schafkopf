export interface Player {
  id: string
  hand: Card[]
}

export interface Card {
  rank: Ranks
  suit: Suits
}

export enum Ranks {
  Sieben = '7',
  Acht = '8',
  Neun = '9',
  Zehn = '10',
  Unter = 'unter',
  Ober = 'ober',
  König = 'könig',
  Sau = 'sau'
}

export enum Suits {
  Eichel = 'eichel',
  Gras = 'gras',
  Herz = 'herz',
  Schelln = 'schelln'
}

export enum PossibleGames {
  Sauspiel = 'Sauspiel',
  Farbsolo = 'Farbsolo',
  Wenz = 'Wenz',
  Farbwenz = 'Farbwenz',
  Geier = 'Geier',
  Farbgeier = 'Farbgeier'
}

export interface Game {
  name: PossibleGames
  trumps: {
    rank: Ranks[]
    suit?: Suits
  }
  suits: Suits[]
  nonTrumpRanks: Ranks[]
}

export interface Bid {
  player: string
  hasGame: boolean
  game?: keyof typeof PossibleGames
}

export const deck: Card[] = [
  { suit: Suits.Eichel, rank: Ranks.Ober },
  { suit: Suits.Eichel, rank: Ranks.Unter },
  { suit: Suits.Eichel, rank: Ranks.König },
  { suit: Suits.Eichel, rank: Ranks.Sau },
  { suit: Suits.Eichel, rank: Ranks.Zehn },
  { suit: Suits.Eichel, rank: Ranks.Neun },
  { suit: Suits.Eichel, rank: Ranks.Acht },
  { suit: Suits.Eichel, rank: Ranks.Sieben },
  { suit: Suits.Gras, rank: Ranks.Ober },
  { suit: Suits.Gras, rank: Ranks.Unter },
  { suit: Suits.Gras, rank: Ranks.König },
  { suit: Suits.Gras, rank: Ranks.Sau },
  { suit: Suits.Gras, rank: Ranks.Zehn },
  { suit: Suits.Gras, rank: Ranks.Neun },
  { suit: Suits.Gras, rank: Ranks.Acht },
  { suit: Suits.Gras, rank: Ranks.Sieben },
  { suit: Suits.Herz, rank: Ranks.Ober },
  { suit: Suits.Herz, rank: Ranks.Unter },
  { suit: Suits.Herz, rank: Ranks.König },
  { suit: Suits.Herz, rank: Ranks.Sau },
  { suit: Suits.Herz, rank: Ranks.Zehn },
  { suit: Suits.Herz, rank: Ranks.Neun },
  { suit: Suits.Herz, rank: Ranks.Acht },
  { suit: Suits.Herz, rank: Ranks.Sieben },
  { suit: Suits.Schelln, rank: Ranks.Ober },
  { suit: Suits.Schelln, rank: Ranks.Unter },
  { suit: Suits.Schelln, rank: Ranks.König },
  { suit: Suits.Schelln, rank: Ranks.Sau },
  { suit: Suits.Schelln, rank: Ranks.Zehn },
  { suit: Suits.Schelln, rank: Ranks.Neun },
  { suit: Suits.Schelln, rank: Ranks.Acht },
  { suit: Suits.Schelln, rank: Ranks.Sieben }
]

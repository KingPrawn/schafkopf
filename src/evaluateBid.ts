import { Bid } from './types/types'
import { gamesHierarchy } from './gameObjects'

export const evaluateBid = (bids: Bid[]) => {
  const hierarchy = gamesHierarchy
  // TODO: fix "any" type at some point.
  const matchGame = (biddedGame: any) => {
    if (hierarchy.findIndex((game) => game === biddedGame) === -1) {
      return 99
    } else {
      return hierarchy.findIndex((game) => game === biddedGame)
    }
  }
  const matches = bids.map((bid) => {
    return matchGame(bid.game)
  })
  return bids.find(bid => bid.game === hierarchy[Math.min(...matches)])
}

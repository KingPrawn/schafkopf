import { Game, Ranks, Suits, PossibleGames } from './types/types'

export const deck = [
  { suit: Suits.Eichel, rank: Ranks.Ober },
  { suit: Suits.Eichel, rank: Ranks.Unter },
  { suit: Suits.Eichel, rank: Ranks.König },
  { suit: Suits.Eichel, rank: Ranks.Sau },
  { suit: Suits.Eichel, rank: Ranks.Zehn },
  { suit: Suits.Eichel, rank: Ranks.Neun },
  { suit: Suits.Eichel, rank: Ranks.Acht },
  { suit: Suits.Eichel, rank: Ranks.Sieben },
  { suit: Suits.Gras, rank: Ranks.Ober },
  { suit: Suits.Gras, rank: Ranks.Unter },
  { suit: Suits.Gras, rank: Ranks.König },
  { suit: Suits.Gras, rank: Ranks.Sau },
  { suit: Suits.Gras, rank: Ranks.Zehn },
  { suit: Suits.Gras, rank: Ranks.Neun },
  { suit: Suits.Gras, rank: Ranks.Acht },
  { suit: Suits.Gras, rank: Ranks.Sieben },
  { suit: Suits.Herz, rank: Ranks.Ober },
  { suit: Suits.Herz, rank: Ranks.Unter },
  { suit: Suits.Herz, rank: Ranks.König },
  { suit: Suits.Herz, rank: Ranks.Sau },
  { suit: Suits.Herz, rank: Ranks.Zehn },
  { suit: Suits.Herz, rank: Ranks.Neun },
  { suit: Suits.Herz, rank: Ranks.Acht },
  { suit: Suits.Herz, rank: Ranks.Sieben },
  { suit: Suits.Schelln, rank: Ranks.Ober },
  { suit: Suits.Schelln, rank: Ranks.Unter },
  { suit: Suits.Schelln, rank: Ranks.König },
  { suit: Suits.Schelln, rank: Ranks.Sau },
  { suit: Suits.Schelln, rank: Ranks.Zehn },
  { suit: Suits.Schelln, rank: Ranks.Neun },
  { suit: Suits.Schelln, rank: Ranks.Acht },
  { suit: Suits.Schelln, rank: Ranks.Sieben }
]

export const possibleGames: Game[] = [
  {
    name: PossibleGames.Sauspiel,
    trumps: {
      rank: [Ranks.Ober, Ranks.Unter],
      suit: Suits.Herz
    },
    suits: [Suits.Eichel, Suits.Gras, Suits.Herz, Suits.Schelln],
    nonTrumpRanks: [
      Ranks.Sau,
      Ranks.Zehn,
      Ranks.König,
      Ranks.Neun,
      Ranks.Acht,
      Ranks.Sieben
    ]
  },
  {
    name: PossibleGames.Farbsolo,
    trumps: {
      rank: [Ranks.Ober, Ranks.Unter],
      suit: Suits.Eichel || Suits.Gras || Suits.Herz || Suits.Schelln
    },
    suits: [Suits.Eichel, Suits.Gras, Suits.Herz, Suits.Schelln],
    nonTrumpRanks: [
      Ranks.Sau,
      Ranks.Zehn,
      Ranks.König,
      Ranks.Neun,
      Ranks.Acht,
      Ranks.Sieben
    ]
  },
  {
    name: PossibleGames.Wenz,
    trumps: {
      rank: [Ranks.Unter]
    },
    suits: [Suits.Eichel, Suits.Gras, Suits.Herz, Suits.Schelln],
    nonTrumpRanks: [
      Ranks.Ober,
      Ranks.Sau,
      Ranks.Zehn,
      Ranks.König,
      Ranks.Neun,
      Ranks.Acht,
      Ranks.Sieben
    ]
  },
  {
    name: PossibleGames.Farbwenz,
    trumps: {
      rank: [Ranks.Unter],
      suit: Suits.Eichel || Suits.Gras || Suits.Herz || Suits.Schelln
    },
    suits: [Suits.Eichel, Suits.Gras, Suits.Herz, Suits.Schelln],
    nonTrumpRanks: [
      Ranks.Ober,
      Ranks.Sau,
      Ranks.Zehn,
      Ranks.König,
      Ranks.Neun,
      Ranks.Acht,
      Ranks.Sieben
    ]
  },
  {
    name: PossibleGames.Geier,
    trumps: {
      rank: [Ranks.Ober]
    },
    suits: [Suits.Eichel, Suits.Gras, Suits.Herz, Suits.Schelln],
    nonTrumpRanks: [
      Ranks.Unter,
      Ranks.Sau,
      Ranks.Zehn,
      Ranks.König,
      Ranks.Neun,
      Ranks.Acht,
      Ranks.Sieben
    ]
  },
  {
    name: PossibleGames.Farbgeier,
    trumps: {
      rank: [Ranks.Ober],
      suit: Suits.Eichel || Suits.Gras || Suits.Herz || Suits.Schelln
    },
    suits: [Suits.Eichel, Suits.Gras, Suits.Herz, Suits.Schelln],
    nonTrumpRanks: [
      Ranks.Unter,
      Ranks.Sau,
      Ranks.Zehn,
      Ranks.König,
      Ranks.Neun,
      Ranks.Acht,
      Ranks.Sieben
    ]
  }
]

export const gamesHierarchy = [
  PossibleGames.Farbsolo,
  PossibleGames.Wenz,
  PossibleGames.Geier,
  PossibleGames.Farbwenz,
  PossibleGames.Farbgeier,
  PossibleGames.Sauspiel
]
import { Card, Player } from './types/types'
import { deck } from './gameObjects'
import { Socket } from 'socket.io'
import { io } from './server'

// export const startGame = async (socket: Socket, room: string) => {
//   const clientsInRoom = await io.in(room).allSockets()
//   if (clientsInRoom.size >= 4) { socket.disconnect } else {
//     console.log(`${socket.id} made it to HERE.`)
//     let counter = 1
//
//     counter++
//     console.log(players.length)
//     if (players.length === 4) {
//       dealCards(deck, players)
//       players.forEach(player => {
//         io.to(player.id).emit('handDealt', player.hand)
//       })
//       // console.log(JSON.stringify(players, null, 2))
//       await bidding(socket, room)
//     }
// }}

export const dealCards = (players: Player[]) => {
  players.forEach(player => player.hand = [])
  const shuffledDeck = [...deck].sort(() => 0.5 - Math.random())
  players.forEach((player: Player) => {
    player.hand.push(...shuffledDeck.splice(0, 8))
    io.to(player.id).emit('handDealt', player.hand)
  })
}

const bidding = async (socket: Socket, room: string) => {
  console.log(`${socket.id} called bidding function`)
  io.in(room).emit('askIfGame', 'Spiel?')
  const games: any = []
  socket.on('hasGame?', (data: any, callback: any) => {
    console.log(data)
    games.push(data)
    callback({
      status: 'ok'
    })
  })
  console.log(`GAMES: ${games}`)
  // if (games.length === 4) {
  //   console.log(games)
  // }
}
// export const startGame = (socket: Socket) => {
//   socket.on('joinRoom', (room) =>  {
//     socket.join(room)
//     console.log(`${socket.id} joined ${room}.`)
//   })
// }

// export const joinLobby = (socket: Socket) => {
//   socket.join('lobby')
//   const msg = `${socket.id} joined lobby.`
//   console.log(msg)
//   socket.to('lobby').emit('lobbyjoin', msg)
// }

// export const joinRoom = (socket: Socket, room: string) => {
//   socket.join('asdf')
//   const msg = `${socket.id} joined ${room}.`
//   console.log(msg)
//   // socket.to(room).emit('roomjoin', msg)
// }

// io.on('connection', async (socket: Socket) => {
//   const clients = await io.in(room).fetchSockets()
//   const numClients = clients.length
//   if (numClients < 4) {
//     await socket.join(room)
//     io.to(room).emit('join', `${socket.id} has joined ${room}`)
//     console.log(`Number of players: ${numClients}`)
//     // console.log(clients)
//   } else if (numClients === 4) {
//     await socket.join(room)
//     io.to(room).emit(`${socket.id} has joined ${room}`)
//     console.log(`Number of players: ${numClients}`)
//     console.log('4 players here, starting game..')
//     io.sockets.in(room).emit('Number of players reached, starting game.')
//     const players = []
//     for (const client of clients) {
//       players.push(client.id as string)
//     }
//     startGame(players)
//   } else {
//     console.log('Room is full.')
//     console.log(numClients)
//     socket.disconnect()
//   }
// })
// export const startGame = async (players: string[]) => {
//   const playerObjs = dealCards(deck, players)
//   playerObjs.forEach(player => { io.to(player.id).emit('dealt', JSON.stringify(player.hand)) })
//   // TODO: Players have to stay in the same order all the time!
//   // At the moment, the order is set by when the room is joined.
//   // Is this fine or do I need some shuffling?
//   // askForPlays(playerObjs)
//   // TODO: When the play is decided, the cards have to be updated
//   // with their
// }

// const askForPlays = (players: Player[]) => {
// }

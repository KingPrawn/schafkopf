import { Card, Game, Suits } from "./types/types"
import { deepEqual } from "./utils/deepEqual"


const isTrump = (card: Card, game: Game) => {
  if (game.trumps.suit) {
    return game.trumps.rank.includes(card.rank) || game.trumps.suit.includes(card.suit) ? true : false
  } else { return game.trumps.rank.includes(card.rank) ? true : false }
}

const isTrickSuit = (card: Card, suit: Suits) => {
  return card.suit === suit ? true : false
}

const buildTrumpHierarchy = (game: Game) => {
  const trumpHierarchy = []
  for (const rank of game.trumps.rank) {
    for (const suit of game.suits) {
      trumpHierarchy.push({ rank, suit })
    }
  }
  if (game.trumps.suit) { game.nonTrumpRanks.forEach(rank => trumpHierarchy.push({ rank, suit: game.trumps.suit })) }
  return trumpHierarchy
}

const buildNonTrumpHierarchy = (game: Game, suit: Suits) => {
  const nonTrumpHierarchy = []
  for (const rank of game.nonTrumpRanks) {
    nonTrumpHierarchy.push({ rank, suit })
  }
  return nonTrumpHierarchy
}

const findWinner = (cards: Card[], hierarchy: Card[]): Card => {
  const matchCard = (playedCard: Card) => { return hierarchy.findIndex(card => deepEqual(card, playedCard)) }
  const matches = cards.map(card => { return matchCard(card) })
  return hierarchy[(Math.min(...matches))]
}

export const evaluateTrick = (trick: Card[], game: Game) => {
  const trumps = trick.filter(card => isTrump(card, game))
  if (trumps.length === 1) {
    return trumps[0]
  } else if (trumps.length > 1) {
    const hierarchy = buildTrumpHierarchy(game)
    return findWinner(trumps, hierarchy)
  } else {
    const trickSuit = trick[0].suit
    const trickSuitCards = trick.filter(card => isTrickSuit(card, trickSuit))
    const hierarchy = buildNonTrumpHierarchy(game, trickSuit)
    return findWinner(trickSuitCards, hierarchy)
  }
}

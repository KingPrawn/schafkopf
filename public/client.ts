const socket = io();

const appendToLog = (msg) => {
  const log = document.getElementById('log')
  const newMsg = document.createElement('div')
  newMsg.innerHTML = `${new Date}::: ${msg}<br>`
  while (newMsg.firstChild) {
    log.appendChild(newMsg.firstChild)
  }
}

const el = (tag, attributes, children) => {
  const element = document.createElement(tag)
  if (attributes) {
    for (const attrName in attributes) {
      element.setAttribute(attrName, attributes[attrName])
    }
  }
  if (children) {
    children.forEach(child => {
      if (typeof child === 'string') {
        element.appendChild(document.createTextNode(child))
      } else {
        element.appendChild(child)
      }
    })
  }
  return element
}
const div = (a, c) => el('div', a, c)

// const el = (tag, attributes, children) => {
//   const element = document.createElement(tag)
//   if (attributes) {
//     for (const attrName in attributes) {
//       element.setAttribute(attrName, attributes[attrName])
//     }
//   }
//   if (children) {
//     children.forEach(child => {
//       if (typeof child === 'string') {
//         element.appendChild(document.createTextNode(child))
//       } else {
//         element.appendChild(child)
//       }
//     })
//   }
//   return element
// }
// const div = (a, c) => el('div', a, c)

const playerInfoBox = document.getElementById('playerInfoBox')
const playArea = document.getElementById('playArea')

socket.on('playerJoined', (socketId, players) => {
  playerInfoBox.textContent = `${socketId} joined room.`
  players.forEach(player => {
    socket.emit('myOwnId?')
    socket.on('yourOwnId', id => {
      console.log(`YOUR OWN ID: ${id}`)
    })
    socket.emit('seating?', players)
    socket.on('seatingSelfPov', relativeSeating => {
      const divIds = [
        { id: 'playerBottom' }, { id: 'playerLeft' }, { id: 'playerTop' }, { id: 'playerRight' }
      ]
      relativeSeating.forEach(seat => {
        const attribute = divIds[relativeSeating.indexOf(seat)]
        playArea.appendChild(div(attribute))
        const { id } = attribute
        const playerId = document.getElementById(id)
        playerId.innerHTML = seat.id
      })
    })
  })
})

socket.on('handDealt', (arg) => {
  if (document.querySelectorAll('.hand').length) {
    document.querySelectorAll('.hand').forEach(el => { el.remove() })
  }
  const hand = div({ class: 'hand' })
  arg.forEach(card => {
    const attributes = { ...card, ...{ class: 'card' } }
    console.log(attributes)
    hand.appendChild(div(attributes))
  })
  document.getElementById('playArea').appendChild(hand)
})

const giveGameChoice = (id, text, hasGame = true) => {
  const button = document.createElement("button")
  button.id = id
  button.innerHTML = text
  button.setAttribute('class', 'choiceBtn')
  playerInfoBox.appendChild(button)
  document.getElementById(id).onclick = () => {
    const data = {
      ...{ player: socket.id, hasGame },
      ...(button.innerHTML != 'Weiter') && { game: button.innerHTML }
    }
    socket.emit('hasGame?', data)
    document.querySelectorAll('button.choiceBtn').forEach(el => el.disabled = true)
  }
}


// TODO: Refactor giveSuitChoice and giveSauChoice and all that leads up to it
const giveSuitChoice = (id, text) => {
  const button = document.createElement("button")
  button.id = id
  button.innerHTML = text
  button.setAttribute('class', 'suitChoiceBtn')
  playerInfoBox.appendChild(button)
  document.getElementById(id).onclick = () => {
    socket.emit('whichSuit?', { player: socket.id, suit: button.innerHTML })
    document.querySelectorAll('button.suitChoiceBtn').forEach(el => el.disabled = true)
  }
}

const giveSauChoice = (id, text) => {
  const button = document.createElement("button")
  button.id = id
  button.innerHTML = text
  button.setAttribute('class', 'suitChoiceBtn')
  playerInfoBox.appendChild(button)
  document.getElementById(id).onclick = () => {
    socket.emit('whichSau?', { player: socket.id, suit: button.innerHTML })
    document.querySelectorAll('button.suitChoiceBtn').forEach(el => el.disabled = true)
  }
}

socket.on('askIfGame', (arg) => {
  playerInfoBox.textContent = arg
  giveGameChoice('weiterBtn', 'Weiter', false)
  giveGameChoice('sauspielBtn', 'Sauspiel')
  giveGameChoice('soloBtn', 'Farbsolo')
  giveGameChoice('wenzBtn', 'Wenz')
  giveGameChoice('geierBtn', 'Geier')
})

socket.on('askForSuit', (arg) => {
  playerInfoBox.textContent = arg
  const suits = ['eichel', 'gras', 'herz', 'schelln']
  suits.forEach(suit => {
    giveSuitChoice(`${suit}Btn`, `${suit.replace(/^\w/, (firstChar) => firstChar.toUpperCase())}`)
  })
})

socket.on('askForSau', (arg) => {
  playerInfoBox.textContent = arg
  const suits = ['eichel', 'gras', 'schelln']
  suits.forEach(suit => {
    giveSauChoice(`${suit}Btn`, `${suit.replace(/^\w/, (firstChar) => firstChar.toUpperCase())}`)
  })
})

socket.on('startTricks', (msg, firstPlayer) => {
  const text = `${msg} ${firstPlayer}`
  playerInfoBox.textContent = text
})
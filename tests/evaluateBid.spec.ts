import { evaluateBid } from '../src/evaluateBid'
import { Bid } from './types/types'

const bid1: Bid[] = [
  { player: 'Alfons', hasGame: false },
  { player: 'Bertl', hasGame: true, game: 'Farbsolo' },
  { player: 'Chris', hasGame: false },
  { player: 'Done', hasGame: true, game: 'Geier' }
]

const bid2: Bid[] = [
  { player: 'Alfons', hasGame: true, game: 'Farbsolo' },
  { player: 'Bertl', hasGame: true, game: 'Farbsolo' },
  { player: 'Chris', hasGame: false },
  { player: 'Done', hasGame: true, game: 'Sauspiel' }
]
const bid3: Bid[] = [
  { player: 'Alfons', hasGame: true, game: 'Sauspiel' },
  { player: 'Bertl', hasGame: true, game: 'Wenz' },
  { player: 'Chris', hasGame: true, game: 'Wenz' },
  { player: 'Done', hasGame: true, game: 'Farbwenz' }
]
const bid4: Bid[] = [
  { player: 'Alfons', hasGame: false },
  { player: 'Bertl', hasGame: false },
  { player: 'Chris', hasGame: true, game: 'Farbsolo' },
  { player: 'Done', hasGame: true, game: 'Geier' }
]
const bid5: Bid[] = [
  { player: 'Alfons', hasGame: false },
  { player: 'Bertl', hasGame: false },
  { player: 'Chris', hasGame: false },
  { player: 'Done', hasGame: true, game: 'Sauspiel' }
]
describe('Test some bids.', () => {
  test.each([
    [1, bid1, { player: 'Bertl', hasGame: true, game: 'Farbsolo' }],
    [1, bid2, { player: 'Alfons', hasGame: true, game: 'Farbsolo' }],
    [1, bid3, { player: 'Bertl', hasGame: true, game: 'Wenz' }],
    [1, bid4, { player: 'Chris', hasGame: true, game: 'Farbsolo' }],
    [1, bid5, { player: 'Done', hasGame: true, game: 'Sauspiel' }]
  ])('%i', (num, bid, expectedWinnerBid) => {
    expect(evaluateBid(bid)).toEqual(expectedWinnerBid)
  })
})

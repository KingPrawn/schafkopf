import { Card, Suits, Ranks } from '../../src/types/types'

export const allTrumps: Card[] = [
  {
    suit: Suits.Herz,
    rank: Ranks.Zehn
  },
  {
    suit: Suits.Eichel,
    rank: Ranks.Unter
  },
  {
    suit: Suits.Gras,
    rank: Ranks.Ober
  },
  {
    suit: Suits.Herz,
    rank: Ranks.Unter
  }
]

export const noTrumps: Card[] = [
  {
    suit: Suits.Eichel,
    rank: Ranks.Sieben
  },
  {
    suit: Suits.Eichel,
    rank: Ranks.Neun
  },
  {
    suit: Suits.Gras,
    rank: Ranks.Sau
  },
  {
    suit: Suits.Eichel,
    rank: Ranks.Acht
  }
]

export const mixed: Card[] = [
  {
    suit: Suits.Herz,
    rank: Ranks.Zehn
  },
  {
    suit: Suits.Eichel,
    rank: Ranks.Unter
  },
  {
    suit: Suits.Gras,
    rank: Ranks.König
  },
  {
    suit: Suits.Gras,
    rank: Ranks.Sieben
  }
]

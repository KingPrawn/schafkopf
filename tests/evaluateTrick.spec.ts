import { evaluateTrick } from '../src/evaluateTrick'
import { allTrumps, noTrumps, mixed } from './data/data'
import { possibleGames } from '../src/gameObjects'
import { Game, PossibleGames, Ranks, Suits } from '../src/types/types'

//  test('Return correct winner card.', () => {
//    expect(evaluateTrick(allTrumps, sauSpiel)).toEqual(
//      { suit: 'gras', rank: 'ober'}
//    )
//  })
// test.each<{ trick: Card[]; game: Game; expectedCard: Card }>([
//   { trick: allTrumps, game: sauSpiel, expectedCard: { suit: 'gras', 'ober' } },
//   { trick: noTrumps, game: sauSpiel, expectedCard: { suit: 'gras', rank: 'ober' } },
//   { trick: mixed, game: sauSpiel, expectedCard: { suit: 'gras', rank: 'ober' } },
// ])('$trick in $game returns $expectedCard.', ({ trick, game, expectedCard }) =>
// {
//   expect(evaluateTrick(trick, game)).toEqual(expectedCard)
// }
const eichelSolo: Game = {
  name: PossibleGames.Farbsolo,
  trumps: {
    rank: [Ranks.Ober, Ranks.Unter],
    suit: Suits.Eichel
  },
  suits: [Suits.Eichel, Suits.Gras, Suits.Herz, Suits.Schelln], // hierarchy of trumps by suit
  nonTrumpRanks: [
    Ranks.Sau,
    Ranks.Zehn,
    Ranks.König,
    Ranks.Neun,
    Ranks.Acht,
    Ranks.Sieben
  ]
}
const grasSolo: Game = {
  name: PossibleGames.Farbsolo,
  trumps: {
    rank: [Ranks.Ober, Ranks.Unter],
    suit: Suits.Gras
  },
  suits: [Suits.Eichel, Suits.Gras, Suits.Herz, Suits.Schelln], // hierarchy of trumps by suit
  nonTrumpRanks: [
    Ranks.Sau,
    Ranks.Zehn,
    Ranks.König,
    Ranks.Neun,
    Ranks.Acht,
    Ranks.Sieben
  ]
}
const herzSolo: Game = {
  name: PossibleGames.Farbsolo,
  trumps: {
    rank: [Ranks.Ober, Ranks.Unter],
    suit: Suits.Herz
  },
  suits: [Suits.Eichel, Suits.Gras, Suits.Herz, Suits.Schelln], // hierarchy of trumps by suit
  nonTrumpRanks: [
    Ranks.Sau,
    Ranks.Zehn,
    Ranks.König,
    Ranks.Neun,
    Ranks.Acht,
    Ranks.Sieben
  ]
}

const schellnSolo: Game = {
  name: PossibleGames.Farbsolo,
  trumps: {
    rank: [Ranks.Ober, Ranks.Unter],
    suit: Suits.Schelln
  },
  suits: [Suits.Eichel, Suits.Gras, Suits.Herz, Suits.Schelln], // hierarchy of trumps by suit
  nonTrumpRanks: [
    Ranks.Sau,
    Ranks.Zehn,
    Ranks.König,
    Ranks.Neun,
    Ranks.Acht,
    Ranks.Sieben
  ]
}

const schellnWenz: Game = {
  name: PossibleGames.Farbsolo,
  trumps: {
    rank: [Ranks.Unter],
    suit: Suits.Schelln
  },
  suits: [Suits.Eichel, Suits.Gras, Suits.Herz, Suits.Schelln], // hierarchy of trumps by suit
  nonTrumpRanks: [
    Ranks.Ober,
    Ranks.Sau,
    Ranks.Zehn,
    Ranks.König,
    Ranks.Neun,
    Ranks.Acht,
    Ranks.Sieben
  ]
}

const eichelGeier: Game = {
  name: PossibleGames.Farbgeier,
  trumps: {
    rank: [Ranks.Ober],
    suit: Suits.Eichel
  },
  suits: [Suits.Eichel, Suits.Gras, Suits.Herz, Suits.Schelln], // hierarchy of trumps by suit
  nonTrumpRanks: [
    Ranks.Unter,
    Ranks.Sau,
    Ranks.Zehn,
    Ranks.König,
    Ranks.Neun,
    Ranks.Acht,
    Ranks.Sieben
  ]
}

function chooseGameFromList (gameName: PossibleGames) {
  return possibleGames.filter((obj) => { return obj.name === gameName}).pop()
}
const sauspiel = chooseGameFromList(PossibleGames.Sauspiel)
const wenz = chooseGameFromList(PossibleGames.Wenz)
const geier = chooseGameFromList(PossibleGames.Geier)

describe('Test all available game variants.', () => {
  // it.each`
  //   trick | game | expectedWinnerCard
  //   ${allTrumps} | ${sauSpiel} | ${{ suit:'gras', rank: 'ober' }}
  // `('$trick[0] in $game returns $expectedWinnerCard.', ({ trick, game, expectedWinnerCard }) => {
  //   expect(evaluateTrick(trick, game)).toEqual(expectedWinnerCard)
  // })
  test.each([
    [1, allTrumps, sauspiel, { suit: 'gras', rank: 'ober' }],
    [2, noTrumps, sauspiel, { suit: 'eichel', rank: '9' }],
    [3, mixed, sauspiel, { suit: 'eichel', rank: 'unter' }],
    [4, allTrumps, eichelSolo, { suit: 'gras', rank: 'ober' }],
    [5, noTrumps, eichelSolo, { suit: 'eichel', rank: '9' }],
    [6, mixed, eichelSolo, { suit: 'eichel', rank: 'unter' }],
    [7, allTrumps, grasSolo, { suit: 'gras', rank: 'ober' }],
    [8, noTrumps, grasSolo, { suit: 'gras', rank: 'sau' }],
    [9, mixed, grasSolo, { suit: 'eichel', rank: 'unter' }],
    [10, allTrumps, herzSolo, { suit: 'gras', rank: 'ober' }],
    [11, noTrumps, herzSolo, { suit: 'eichel', rank: '9' }],
    [12, mixed, herzSolo, { suit: 'eichel', rank: 'unter' }],
    [13, allTrumps, schellnSolo, { suit: 'gras', rank: 'ober' }],
    [14, noTrumps, schellnSolo, { suit: 'eichel', rank: '9' }],
    [15, mixed, schellnSolo, { suit: 'eichel', rank: 'unter' }],
    [16, allTrumps, wenz, { suit: 'eichel', rank: 'unter' }],
    [17, noTrumps, wenz, { suit: 'eichel', rank: '9' }],
    [18, mixed, wenz, { suit: 'eichel', rank: 'unter' }],
    [19, allTrumps, schellnWenz, { suit: 'eichel', rank: 'unter' }],
    [20, noTrumps, schellnWenz, { suit: 'eichel', rank: '9' }],
    [22, mixed, schellnWenz, { suit: 'eichel', rank: 'unter' }],
    [23, allTrumps, geier, { suit: 'gras', rank: 'ober' }],
    [24, noTrumps, geier, { suit: 'eichel', rank: '9' }],
    [25, mixed, geier, { suit: 'herz', rank: '10' }],
    [26, allTrumps, eichelGeier, { suit: 'gras', rank: 'ober' }],
    [27, noTrumps, eichelGeier, { suit: 'eichel', rank: '9' }],
    [28, mixed, eichelGeier, { suit: 'eichel', rank: 'unter' }]
  ])('%i', (num, trick, game, expectedWinnerCard) => {
    expect(evaluateTrick(trick, game as Game)).toEqual(expectedWinnerCard)
  })
})

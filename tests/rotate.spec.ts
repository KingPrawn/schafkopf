import { rotate } from '../src/utils/rotate'

let array: number[] = []
let arrayOfObjs: any[] = []

beforeEach(() => {
  array = [1, 2, 3, 4]
  arrayOfObjs = [{ a: 1 }, { b: 2 }, { c: 3 }]
})

describe('Rotates one time...', () => {
  test('...array of numbers.', () => {
    expect(rotate(array, 1)).toEqual([4, 1, 2, 3])
  })
  test('...array of objects.', () => {
    expect(rotate(arrayOfObjs, 1)).toEqual([{ c: 3 }, { a: 1 }, { b: 2 }])
  })
})
describe('Rotates two times...', () => {
  test('...array of numbers.', () => {
    expect(rotate(array, 2)).toEqual([3, 4, 1, 2])
  })
  test('...array of objects...', () => {
    expect(rotate(arrayOfObjs, 2)).toEqual([{ b: 2 }, { c: 3 }, { a: 1 }])
  })
})
describe('Rotates three times...', () => {
  test('...array of numbers.', () => {
    expect(rotate(array, 3)).toEqual([2, 3, 4, 1])
  })
  test('...array of objects.', () => {
    expect(rotate(arrayOfObjs, 3)).toEqual([{ a: 1 }, { b: 2 }, { c: 3 }])
  })
})
describe('Rotates four times...', () => {
  test('...array of numbers.', () => {
    expect(rotate(array, 4)).toEqual([1, 2, 3, 4])
  })
  test('...array of objects.', () => {
    expect(rotate(arrayOfObjs, 4)).toEqual([{ c: 3 }, { a: 1 }, { b: 2 }])
  })
})

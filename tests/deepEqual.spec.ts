import { deepEqual } from '../src/utils/deepEqual'

describe('Test deepEqual function.', () => {
  test('Same objects with same property order are equal.', () => {
    const obj1 = {
      a: 'a',
      b: 'b'
    }
    const obj2 = {
      a: 'a',
      b: 'b'
    }
    expect(deepEqual(obj1, obj2)).toBeTruthy()
  })
  test('Same objects with different property order are equal.', () => {
    const obj1 = {
      a: 'a',
      b: 'b'
    }
    const obj2 = {
      b: 'b',
      a: 'a'
    }
    expect(deepEqual(obj1, obj2)).toBeTruthy()
  })
  test('Different key names are recognized.', () => {
    const obj1 = {
      a: 'a',
      b: 'b'
    }
    const obj2 = {
      c: 'c',
      d: 'd'
    }
    expect(deepEqual(obj1, obj2)).toBeFalsy()
  })
  test('Different property number is recognized.', () => {
    const obj1 = {
      a: 'a',
      b: 'b'
    }
    const obj2 = {
      a: 'a',
    }
    expect(deepEqual(obj1, obj2)).toBeFalsy()
  })
})

